Operate 24 hours a day from Walthamstow in East London. We cover both domestic and commercial clients for either emergency requests or by appointment for all general work requests from 9 am – 5 pm.

Address: 6 Salop Rd, Walthamstow, London,  E17 7HT, UK

Phone: +44 20 8521 9518

Website: https://platinumemergency.com
